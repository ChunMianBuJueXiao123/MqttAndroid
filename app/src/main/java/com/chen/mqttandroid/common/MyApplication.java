package com.chen.mqttandroid.common;

import android.app.Application;

/**
 * Created by chen on 2018/5/11.
 */

public class MyApplication extends Application {
    private static final String TAG = "JIGUANG-Example";

    private final String ip = "http://192.168.124.9:8282";

    public String getIp() {
        return this.ip;
    }

}