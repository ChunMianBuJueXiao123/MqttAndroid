package com.chen.mqttandroid.entity;

import java.util.Date;

/**
 * Created by chen on 2018/5/11.
 */

public class DeviceInfo {
    private Long id;//设备Id
    private String did;//设备uuid
    private String pid;//对应产品ID
    private int state;//设备状态
    private Date createTime;//创建时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
