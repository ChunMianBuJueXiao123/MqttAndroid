package com.chen.mqttandroid.entity;

import java.util.Date;

/**
 * Created by chen on 2018/5/11.
 */

public class DataPointInfo {
    private Long id;//数据点Id
    private String name;//数据点名称
    private int type;//数据点类型，记录（记录大小和时间戳），状态（记录状态，最后时间戳）
    private Date createTime;//创建时间
    private String pid;//产品ID

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
