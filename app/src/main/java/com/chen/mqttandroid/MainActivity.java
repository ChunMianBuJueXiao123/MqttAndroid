package com.chen.mqttandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {


    private String TAG = MainActivity.class.getSimpleName();
    private HomeFragment mClassFragment;
    private HomeFragment mMyFragment;
    private HomeFragment mHomeFragment;

    private LinearLayout mTabExamLayout;
    private LinearLayout mTabClassLayout;
    private LinearLayout mTabSettingLayout;

    private ImageButton mTabExamImgButton;
    private ImageButton mTabClassImgButton;
    private ImageButton mTabSettingButton;

    //将三个ImageButton置为灰色
    private void resetImgs() {
        mTabExamImgButton.setImageResource(R.drawable.bottombar_homework);
        mTabClassImgButton.setImageResource(R.drawable.bottombar_group);
        mTabSettingButton.setImageResource(R.drawable.bottombar_setting);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    public void initView(){
        mTabExamImgButton = (ImageButton)findViewById(R.id.id_tab_exam_img);
        mTabClassImgButton = (ImageButton)findViewById(R.id.id_tab_class_img);
        mTabSettingButton = (ImageButton)findViewById(R.id.id_tab_setting_img);

        mTabExamLayout = (LinearLayout) findViewById(R.id.id_tab_exam_layout);
        mTabClassLayout = (LinearLayout) findViewById(R.id.id_tab_class_layout);
        mTabSettingLayout = (LinearLayout) findViewById(R.id.id_tab_setting_layout);

        mTabExamLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(0);
            }
        });
        mTabClassLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(1);
            }
        });
        mTabSettingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelect(2);
            }
        });
        setDefaultFragment();//设置默认导航栏
    }


    //设置选中第几页，使用FragmentManager进行控制
    public void setSelect(int position) {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fm.beginTransaction();
        for (int i = 0; i < 3; i++) {
            if (position != i) {
                if (i == 0 && mHomeFragment != null) {
                    transaction.hide(mHomeFragment);
                }
                if (i == 1 && mClassFragment != null) {
                    transaction.hide(mClassFragment);
                }
                if (i == 2 && mMyFragment != null) {
                    transaction.hide(mMyFragment);
                }
            }
        }
        resetImgs();
        switch (position) {
            case 0:
                mTabExamImgButton.setImageResource(R.drawable.bottombar_homework_pressed);
                if (mHomeFragment == null) {
                    mHomeFragment = HomeFragment.newInstance();
                    transaction.add(R.id.fragment_place, mHomeFragment);
                } else {
                    transaction.show(mHomeFragment);
                }
                break;
            case 1:
                mTabClassImgButton.setImageResource(R.drawable.bottombar_group_pressed);
                if (mClassFragment == null) {
                    mClassFragment = HomeFragment.newInstance();
                    transaction.add(R.id.fragment_place, mClassFragment);
                } else {
                    transaction.show(mClassFragment);
                }
                break;
            case 2:
                mTabSettingButton.setImageResource(R.drawable.bottombar_setting_pressed);
                if (mMyFragment == null) {
                    mMyFragment = HomeFragment.newInstance();
                    transaction.add(R.id.fragment_place, mMyFragment);
                } else {
                    transaction.show(mMyFragment);
                }
                break;
            default:
                break;
        }
        transaction.commit();// 事务提交
    }

    /**
     * 设置默认导航栏
     */
    private void setDefaultFragment() {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fm.beginTransaction();
        mHomeFragment = new HomeFragment();
        transaction.replace(R.id.fragment_place, mHomeFragment);
        transaction.commit();
        mTabExamImgButton.setImageResource(R.drawable.bottombar_homework_pressed);
    }

    @Override
    public void onBackPressed() {
        //按Back键实现Home键效果
        //super.onBackPressed();这句话一定要注掉,不然又去调用默认的back处理方式了
        Intent i= new Intent(Intent.ACTION_MAIN);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addCategory(Intent.CATEGORY_HOME);
        startActivity(i);
    }
}
