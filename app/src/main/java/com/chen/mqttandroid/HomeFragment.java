package com.chen.mqttandroid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chen.mqttandroid.common.MyApplication;
import com.chen.mqttandroid.dataactivity.ShowDataActivity;
import com.chen.mqttandroid.entity.DeviceInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 *  主页的Fragment，适配Home的Tab的Homework的Tab
 *
 *  @author chen
 *  @date 2018/5/2
 */

public class HomeFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private ListAllDeviceInfoAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private TextView tipsView;
    private List<DeviceInfo> mDeviceInfoSet;
    private int i = 0;
    private UserGetAllDeviceInfoTask mTask = null;
    private String IP;
    private ProgressDialog waitingDialog;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        i = 0;

        getWaitingDialog();

        //      设置网络无法连接的提示信息
        tipsView = view.findViewById(R.id.home_fragment_tips_network_textview);
        tipsView.setText("网络无法连接");
        tipsView.setVisibility(View.GONE);

        setRecyclerViewAndSwipeRefreshLayout(view);

        //      第一次调用异步请求更新的数据
        swipeRefreshLayout.setRefreshing(true);
        mTask = new UserGetAllDeviceInfoTask(1);
        mTask.execute((Void) null);
        IP = ((MyApplication)getActivity().getApplication()).getIp();
        return view;
    }

    //  设置列表和下拉布局的属性
    public void setRecyclerViewAndSwipeRefreshLayout(View view){
        //      下拉刷新的实现
        swipeRefreshLayout = view.findViewById(R.id.home_fragment_swiperefreshlayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                        tipsView.setVisibility(View.GONE);
                        mTask = new UserGetAllDeviceInfoTask(1);
                        mTask.execute((Void) null);
            }
        });
        mRecyclerView = (RecyclerView) view.findViewById(R.id.home_fragment_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), OrientationHelper.VERTICAL));
        List<DeviceInfo> deviceInfoList = new ArrayList<>();
        DeviceInfo d1 = new DeviceInfo();
        deviceInfoList.add(d1);
        mDeviceInfoSet = deviceInfoList;
        mAdapter = new ListAllDeviceInfoAdapter(mDeviceInfoSet);
        mAdapter.setOnItemClickListener(new ListAllDeviceInfoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                UserGetAllDataPointInfoTask task = new UserGetAllDataPointInfoTask(1,1);
                task.execute((Void) null);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    /**
     *  异步请求班级（或者说个人内）的所有测试信息，用来显示Quiz列表，包括做过的和未做的
     *  参考：https://blog.csdn.net/l1371985810/article/details/50175641
     *
     *  @author chen
     *  @date 2018/4/27
     */
// TODO: 2018/4/27 这里是有问题的是应该用studentId或者classId去请求所有的quiz，还没有确定。
    public class UserGetAllDeviceInfoTask extends AsyncTask<Void,Void,String> {
        //  想要得到的userId
        private long userId;
        //  请求返回的信息
        private String responseData = null;

        public UserGetAllDeviceInfoTask(long userId){
            this.userId = userId;
        }

        // 在后台执行的任务，请求http://192.168.3.119:8282/student/getallquizinfo 通过参数classId
        @Override
        protected String doInBackground(Void... params) {
            Response response = null;
            OkHttpClient okHttpClient = new OkHttpClient();
            //建立请求表单，添加上传服务器的参数
            FormBody formBody = new FormBody.Builder()
                    .add("userId", String.valueOf(this.userId))
                    .build();
            Request request = new Request.Builder()
                    .url("http://192.168.124.9:8282"+"/user/getalldeviceinfo")
                    .post(formBody)
                    .build();
            try {
                response = okHttpClient.newCall(request).execute();
                responseData = response.body().string();
                System.out.println("it is ");
            } catch (IOException e) {
                System.out.println("IO异常");
                responseData = "IO异常";
            }
            return responseData;
        }

        @Override
        protected void onPostExecute(final String data) {
            if (data != "IO异常") {//请求有返回
                List<DeviceInfo> c = new ArrayList<>();
                c = new Gson().fromJson(data,new TypeToken<ArrayList<DeviceInfo>>(){}.getType());
                mDeviceInfoSet = c ;//更新保持数据或者初始化数据
                if(i == 0){//如果是第一次请求则设置显示mRecyclerView
                    i = 1;
                    mAdapter.setmDeviceInfoSet(mDeviceInfoSet);//更新数据
                    mAdapter.notifyDataSetChanged();//通知layoutManager数据已经改变
                    swipeRefreshLayout.setRefreshing(false);//隐藏加载标志
                    mRecyclerView.setVisibility(View.VISIBLE);

                }else {
                    mAdapter.setmDeviceInfoSet(mDeviceInfoSet);//更新数据
                    mAdapter.notifyDataSetChanged();//通知layoutManager数据已经改变
                    swipeRefreshLayout.setRefreshing(false);//隐藏加载标志
                }
            } else {//请求IO异常
                swipeRefreshLayout.setRefreshing(false);//隐藏加载标志
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setIcon(R.mipmap.ic_launcher)//设置标题的图片
                        .setTitle("没有网络")//设置对话框的标题
                        .setMessage("请检查您的网络")//设置对话框的内容
                        //设置对话框的按钮
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
                tipsView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                i = 0;
            }
        }
    }


    private ProgressDialog getWaitingDialog() {
    /* 等待Dialog具有屏蔽其他控件的交互能力
     * @setCancelable 为使屏幕不可点击，设置为不可取消(false)
     * 下载等事件完成后，主动调用函数关闭该Dialog
     */
        waitingDialog=
                new ProgressDialog(getActivity());
        waitingDialog.setMessage("等待中...");
        waitingDialog.setIndeterminate(true);
        waitingDialog.setCancelable(false);
        return waitingDialog;
    }
    private void dismissWaitingDialog(){
        waitingDialog.dismiss();
    }

    /**
     *  异步获取做题的结果从服务器，并返回到ShowExamDetailActivity中
     *  参考：https://blog.csdn.net/l1371985810/article/details/50175641
     *
     *  @author chen
     *  @date 2018/5/4
     */
    public class UserGetAllDataPointInfoTask extends AsyncTask<Void,Void,String> {

        private long did;
        private long pid;
        //  请求返回的信息
        private String responseData = null;

        public UserGetAllDataPointInfoTask(long did, long pid){
            this.did = did;
            this.pid = pid;
        }

        // 在后台执行的任务，请求http://192.168.3.119:8282/student/getallquizinfo 通过参数classId
        @Override
        protected String doInBackground(Void... params) {

            Response response = null;
            OkHttpClient okHttpClient = new OkHttpClient();
            //建立请求表单，添加上传服务器的参数
            FormBody formBody = new FormBody.Builder()
                    .add("pid", String.valueOf(pid))
                    .add("did",String.valueOf(did))
                    .build();
            Request request = new Request.Builder()
                    .url(IP+"/user/getAllDataPointInfo")
                    .post(formBody)
                    .build();
            try {
                response = okHttpClient.newCall(request).execute();
                responseData = response.body().string();
            } catch (IOException e) {
                System.out.println("IO异常");
                responseData = "IO异常";
            }
            return responseData;
        }

        @Override
        protected void onPostExecute(final String data) {
            waitingDialog.dismiss();
            if (data != "IO异常") {//请求有返回,将字符串返回给
                Intent intent = new Intent(getActivity(), ShowDataActivity.class);
                intent.putExtra("userAllDataPointInfoJson",data);
                startActivity(intent);
            } else {//请求IO异常
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setIcon(R.mipmap.ic_launcher)//设置标题的图片
                        .setTitle("没有网络")//设置对话框的标题
                        .setMessage("请检查您的网络")//设置对话框的内容
                        //设置对话框的按钮
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
            }
        }
    }

}


