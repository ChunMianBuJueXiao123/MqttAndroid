package com.chen.mqttandroid;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chen.mqttandroid.entity.DeviceInfo;

import java.util.List;

/**
 * Created by chen on 2018/5/11.
 */

public class ListAllDeviceInfoAdapter extends RecyclerView.Adapter<ListAllDeviceInfoAdapter.ViewHolder>{
    private List<DeviceInfo> mDeviceInfoSet;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView deviceNameView;
        public ViewHolder(View v) {
            super(v);
            deviceNameView = v.findViewById(R.id.device_name_textview);

        }
    }

    //  用于下拉刷新更新数据
    public void setmDeviceInfoSet(List<DeviceInfo> mDeviceInfoSet){
        this.mDeviceInfoSet = mDeviceInfoSet;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListAllDeviceInfoAdapter(List<DeviceInfo> mDeviceInfoSet) {
        this.mDeviceInfoSet = mDeviceInfoSet;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListAllDeviceInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                             int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_all_device_list, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh1= new ViewHolder(v);
        return vh1;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.deviceNameView.setText(String.valueOf(mDeviceInfoSet.get(position).getId()));
        if(onItemClickListener!=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = holder.getLayoutPosition();
                    onItemClickListener.onItemClick(holder.itemView,position);
                }
            });
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDeviceInfoSet.size();
    }

    private OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener){
        this.onItemClickListener = mOnItemClickListener;
    }


    public interface OnItemClickListener{
        void onItemClick(View v,int position);
    }
}
