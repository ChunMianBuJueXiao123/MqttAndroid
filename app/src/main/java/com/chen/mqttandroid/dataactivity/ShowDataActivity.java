package com.chen.mqttandroid.dataactivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chen.mqttandroid.HomeFragment;
import com.chen.mqttandroid.ListAllDeviceInfoAdapter;
import com.chen.mqttandroid.R;
import com.chen.mqttandroid.common.MyApplication;
import com.chen.mqttandroid.entity.DataPointInfo;
import com.chen.mqttandroid.entity.DeviceInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chen on 2018/5/11.
 */

public class ShowDataActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ListAllDataPointInfoAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tipsView;
    private List<DataPointInfo> mDataPointInfoSet;
    private int i = 0;
    private HomeFragment.UserGetAllDeviceInfoTask mTask = null;
    private String IP;
    private ProgressDialog waitingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_data);

        Intent intent = getIntent();
        String userAllDataPointInfoJson = intent.getStringExtra("userAllDataPointInfoJson");
        List<DataPointInfo> c = new ArrayList<>();
        System.out.println(userAllDataPointInfoJson);
        c = new Gson().fromJson(userAllDataPointInfoJson,new TypeToken<ArrayList<DataPointInfo>>(){}.getType());
        mDataPointInfoSet = c;

        setRecyclerViewAndSwipeRefreshLayout();

        IP = ((MyApplication)getApplication()).getIp();

    }

    //  设置列表和下拉布局的属性
    public void setRecyclerViewAndSwipeRefreshLayout(){

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_show_data_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ShowDataActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(ShowDataActivity.this, OrientationHelper.VERTICAL));

        mAdapter = new ListAllDataPointInfoAdapter(mDataPointInfoSet);
        mRecyclerView.setAdapter(mAdapter);
    }
}
