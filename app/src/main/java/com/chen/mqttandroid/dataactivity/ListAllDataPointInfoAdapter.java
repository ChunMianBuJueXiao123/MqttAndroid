package com.chen.mqttandroid.dataactivity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chen.mqttandroid.R;
import com.chen.mqttandroid.entity.DataPointInfo;

import java.util.List;

/**
 * Created by chen on 2018/5/11.
 */

public class ListAllDataPointInfoAdapter extends RecyclerView.Adapter<ListAllDataPointInfoAdapter.ViewHolder>{
    private List<DataPointInfo> mDataPointInfoSet;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView deviceNameView;
        public TextView deviceDataView;
        public ViewHolder(View v) {
            super(v);
            deviceNameView = v.findViewById(R.id.datapoint_name_textview);
            deviceDataView = v.findViewById(R.id.datapoint_data_textview);

        }
    }

    //  用于下拉刷新更新数据
    public void setmDataPointInfoSet(List<DataPointInfo> mDataPointInfoSet){
        this.mDataPointInfoSet = mDataPointInfoSet;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListAllDataPointInfoAdapter(List<DataPointInfo> mDataPointInfoSet) {
        this.mDataPointInfoSet = mDataPointInfoSet;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListAllDataPointInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_all_datapoint_list, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh1= new ViewHolder(v);
        return vh1;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.deviceNameView.setText(String.valueOf(mDataPointInfoSet.get(position).getName()));
        holder.deviceDataView.setText(String.valueOf(mDataPointInfoSet.get(position).getName()));
        if(onItemClickListener!=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = holder.getLayoutPosition();
                    onItemClickListener.onItemClick(holder.itemView,position);
                }
            });
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataPointInfoSet.size();
    }

    private OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener){
        this.onItemClickListener = mOnItemClickListener;
    }


    public interface OnItemClickListener{
        void onItemClick(View v,int position);
    }
}
